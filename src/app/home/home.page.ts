import { Component } from '@angular/core';
import { Chart } from "node_modules/chart.js";
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {

  constructor(public afd : AngularFireDatabase) {}

  // Stores health info retrieved from database
  healthyData;

  ngOnInit(){ 
    this.getHealthyData();
  }

  // Create Overall Health Chart
  // Argument:  info: list;
  overallHealthChart(info) {
    var newData = [info[0], info[1]];
    var ctx = document.getElementById('healthChart');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ['Healthy', 'Unhealthy'],
            datasets: [{
                label: 'Your overall health status:',
                data: newData,
                backgroundColor: [
                    'rgba(62, 237, 68, 1)',
                    'rgba(252, 68, 68, 1)'
                ],
                borderColor: [
                    'rgba(0, 0, 0, 1)',
                    'rgba(0, 0, 0, 1)'
                ],
                borderWidth: 0.3
            }]
        },
        options: {
          title: {
            display: false,
          }
        }
    });
  }

  // Retrieve Data About Health From Datbase
  getHealthyData() {
    this.afd.list('/Overall/Unhealthy/').valueChanges().subscribe(
      data => {
        this.healthyData = data;
        this.overallHealthChart(this.healthyData);

      }
    )
  }

}
