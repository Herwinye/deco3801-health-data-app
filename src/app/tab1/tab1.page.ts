import { Component, OnInit, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { CalendarComponent } from 'ionic2-calendar';
// import { ModalController } from '@ionic/angular/providers/modal-controller';
import { CalModalPage } from '../pages/cal-modal/cal-modal.page';
 // Database
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { config } from 'process';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})

export class Tab1Page implements OnInit {

  eventSource = [];
  viewTitle: string;

  appointmentNo = 15;

  // Variable adding items to database
  appointmentDay;
  appointmentMonth;
  appointmentStartTime;
  appointmentEndTime;
  appointmentType: string;

  calendar ={
    mode: 'month',
    currentDate: new Date(),
  };

  @ViewChild(CalendarComponent) myCal : CalendarComponent;

  constructor(public afd : AngularFireDatabase) {}

  // Variables for appointments
  appointments;

  ngOnInit(){
    // Get Appointments From Database And Populate Calendar
    this.getAppointments();
  }

  next(){
    this.myCal.slideNext();
  }
  back(){
    this.myCal.slidePrev();
  }
  onViewTitleChanged(title){
    this.viewTitle = title;
  }

  onEventSelected(event) {
    console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
   }

  onRangeChanged(ev) {
    console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
  }
  
    // Create Single Event Based On Database
    createEvent(type, description, location, date, time) {
        var newEvent = [];
        var startTime;
        var endTime;
        startTime = new Date(date[2], date[1], date[0], time[1], 0, 0, 0);
        endTime = new Date(date[2], date[1], date[0], time[0], 0, 0, 0);
        newEvent.push({
            title: type,
            startTime: startTime,
            endTime: endTime,
            allDay: false
        });
        this.eventSource = newEvent;
    }

    // Create All Events In Database
    createMultipleEvents(allAppointments) {
      var newEvent = [];
      var startTime;
      var endTime;
      var singleAppointment;
      for (singleAppointment in allAppointments) {
        var appointment = allAppointments[singleAppointment];
        var type = appointment["Type"];
        var date = appointment["Date"];
        var time = appointment["Time"];
        startTime = new Date(date["Year"], date["Month"]-1, date["Day"], time["Start"], 0, 0, 0);
        endTime = new Date(date["Year"], date["Month"]-1, date["Day"], time["End"], 0, 0, 0);
        newEvent.push({
          title: type,
          startTime: startTime,
          endTime: endTime,
          allDay: false
        });

        }
     this.eventSource = newEvent;
  }

  // Get Data from Database & Populate Calendar
    getAppointments() {
      this.afd.object('/NewEvents/').valueChanges().subscribe(
        data => {
          this.appointments = data;
          console.log(this.appointments);
          this.createMultipleEvents(this.appointments);
        }
      )
    }

    // Add An Appointment To Database
    addAppointmentToDatabase() {

      // Add Date
      this.afd.list("/NewEvents/Appointment"+this.appointmentNo+"/Date/").set("Day", parseInt(this.appointmentDay));
      this.afd.list("/NewEvents/Appointment"+this.appointmentNo+"/Date/").set("Month", parseInt(this.appointmentMonth));
      this.afd.list("/NewEvents/Appointment"+this.appointmentNo+"/Date/").set("Year", 2020);
      // Add Type
      this.afd.list("/NewEvents/Appointment"+this.appointmentNo+"/").set("Type", this.appointmentType);
      // Add Time
      this.afd.list("/NewEvents/Appointment"+this.appointmentNo+"/Time/").set("Start", parseInt(this.appointmentStartTime));
      this.afd.list("/NewEvents/Appointment"+this.appointmentNo+"/Time/").set("End", parseInt(this.appointmentEndTime));
      // Add Location
      this.afd.list("/NewEvents/Appointment"+this.appointmentNo+"/").set("Location", "Mt. Gravatt");

      // Increment Appointment No.
      this.appointmentNo = this.appointmentNo + 1;
      this.getAppointments();
      this.addedAppointmentNotification();
    }

    // Show Notification When Appointment's Added
    addedAppointmentNotification() {
      document.getElementById("added-appointment").style.display = "block";
      setTimeout(() => {
        document.getElementById("added-appointment").style.display = "none";
      }, 2500);
    }
}
