import { Component } from '@angular/core';
import { Chart } from "node_modules/chart.js";
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})

export class Tab3Page {

  constructor(public afd : AngularFireDatabase) {}

  // Default location to show on map
  lat = -27.47;
  lng = 153.1;
  zoom = 23;

  // Data Retrieved From Database Stored Here
  heartData;
  hearingData;

  // Data Structure Containing All Hospitals To Show
  private mylocations = [
      { lat: -27.4965478, lng: 153.0265318},
      { lat: -27.5105383, lng: 153.0404548},
      { lat: -27.485, lng: 153.0258669},
      { lat: -27.4778552,lng: 152.9891829},
      { lat: -27.4857824,lng: 152.9923617},
      { lat: -27.4674461,lng: 153.0140506},
      { lat: -27.4839778,lng: 153.0265697}

  ];

  // Page Title, pretty self-explanatory innit!
  page_title: string;

  ngOnInit() {
    this.page_title = "More";
    this.getHeartDataFromDatabase();
    this.getHearingDataFromDatabase();
  }

  // Go Back to the main section
  goBack() {
    this.removeOtherFeaturesSection();
    document.getElementById("other-features-section").style.display = "block";
    this.page_title = "More";
  }

  // Display Different Sections and Change Their Headings
  displayPrescriptionsSection() {
    this.removeOtherFeaturesSection();
    this.page_title = "Prescriptions";
    document.getElementById("prescriptions-section").style.display = "block";
  }

  displayHealthInstitutionsSection() {
    this.removeOtherFeaturesSection();
    this.page_title = "Institutions";
    document.getElementById("health-institutions-section").style.display = "block";
  }

  displayHeartHealthSection() {
    this.removeOtherFeaturesSection();
    this.page_title = "Heart Health";
    document.getElementById("heartHealth-section").style.display = "block";
    }

  displayHearingHealthSection() {
    this.removeOtherFeaturesSection();
    this.page_title = "Hearing";
    document.getElementById("hearingHealth-section").style.display = "block";
  }

  // Stop Dispaying Every Section
  removeOtherFeaturesSection() {
    document.getElementById("other-features-section").style.display = "none";
    document.getElementById("prescriptions-section").style.display = "none";
    document.getElementById("health-institutions-section").style.display = "none";
    document.getElementById("heartHealth-section").style.display="none";
    document.getElementById("hearingHealth-section").style.display = "none";
  }

  // Create Charts For "Heart Health" And "Hearing Health"
  heartChart(info) {
    var ctx = document.getElementById('heartChart');
    var newInfo = [info[1], info[5], info[6], info[4], info[0], info[2], info[3]]
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            datasets: [{
                label: 'Heart Results',
                data: newInfo,
                backgroundColor: [
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)'
                ],
                borderColor: [
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
          legend: {
        display: false
    },
    title: {
            display: true,
            text: 'Heart Rate',
            fontSize: 16,
        },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 70
                    }
                }]
            }
        }
    });
  }

  hearingChart(info) {
    var ctx = document.getElementById('hearingChart');
    var newInfo = [info[1], info[5], info[6], info[4], info[0], info[2], info[3]]
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            datasets: [{
                label: 'Headphone Volume',
                data: newInfo,
                backgroundColor: [
                    'rgba(245, 66, 66, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(255, 162, 0, 1)'
                ],
                borderColor: [
                    'rgba(245, 66, 66, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(255, 162, 0, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
          legend: {
        display: false
    },
    title: {
            display: true,
            text: 'Headphones Volume',
            fontSize: 16,
        },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 100
                    }
                }]
            }
        }
    });
  }

  // Retrieve Data From Database For "Heart Health" And "Hearing Health"

  getHeartDataFromDatabase(){
    this.afd.list('/HeartRate/').valueChanges().subscribe(
      data => {
        this.heartData = data;
        this.heartChart(this.heartData);
      }
    )
  }

  getHearingDataFromDatabase(){
    this.afd.list('/Hearing/').valueChanges().subscribe(
      data => {
        this.hearingData = data;
        this.hearingChart(this.hearingData);
      }
    )
  }
}
