import { Component } from "@angular/core";
import { Chart } from "node_modules/chart.js";
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page {

  constructor(public afd : AngularFireDatabase) {}

  // Are Charts Created ?
  chartsCreated = false;

  // Chart Title
  selected_option: string;

  // Store Data Retrieved From Database
  glucoseData;
  insulinData;
  ironData;
  vitaminData;

  ngOnInit() {

    // Get Data from database
    this.getGlucoseDataFromDatabase();
    this.getInsulinDataFromDatabase();
    this.getIronDataFromDatabase();
    this.getVitaminDataFromDatabase();
    
    // Don't Display Any Graph At Start
    document.getElementById("glucoseChart").style.display = "none";
    document.getElementById("insulinChart").style.display = "none";
    document.getElementById("ironChart").style.display = "none";
    document.getElementById("vitaminChart").style.display = "none";
    document.getElementById("diabetes-info").style.display = "none"
    document.getElementById("insulin-info").style.display = "none"
    document.getElementById("iron-info").style.display = "none"
    document.getElementById("vitaminD-info").style.display = "none"
  }

  // Hide Blood Card !!!!!!!!!
  hideBloodCard() {
    document.getElementById("bloodPageCard").style.display = "none";
  }

  // Show The Selected Chart
  showSelectedChart() {

    // Are Charts Created? - Create charts if not created
    if (!this.chartsCreated) {
      this.createGlucoseChart("glucoseChart", this.glucoseData);
      this.createInsulinChart("insulinChart", this.insulinData);
      this.monthChartIron("ironChart", this.ironData);
      this.monthChartVitamin("vitaminChart", this.vitaminData);
      // Are Charts Created? - Well now they are dumbo!!
      this.chartsCreated = true;
    }

    // Hide All Cards & Charts And Then Show Relevant Chart & Relevant Cards
    if (this.selected_option == "glucose") {
      // Show Glucose Chart
      this.optionChangeRemoval();
      document.getElementById("glucoseChart").style.display = "block";
      document.getElementById("diabetes-info").style.display = "block";
    } else if (this.selected_option == "insulin") {
      // Show Insulin Chart
      this.optionChangeRemoval();
      document.getElementById("insulinChart").style.display = "block";
      document.getElementById("insulin-info").style.display = "block";
    } else if (this.selected_option == "iron") {
      // Show Iron Chart
      this.optionChangeRemoval();
      document.getElementById("ironChart").style.display = "block";
      document.getElementById("iron-info").style.display = "block";
    } else if (this.selected_option == "Vitamin D") {
      // Show Vitamin D Chart
      this.optionChangeRemoval();
      document.getElementById("vitaminChart").style.display = "block";
      document.getElementById("vitaminD-info").style.display = "block";

    } 
  }

  // Hide Every Graph And Card
  optionChangeRemoval() {
    document.getElementById("glucoseChart").style.display = "none";
    document.getElementById("insulinChart").style.display = "none";
    document.getElementById("ironChart").style.display = "none";
    document.getElementById("vitaminChart").style.display = "none";
    document.getElementById("diabetes-info").style.display = "none"
    document.getElementById("insulin-info").style.display = "none"
    document.getElementById("iron-info").style.display = "none"
    document.getElementById("vitaminD-info").style.display = "none"
  }

// Create Graphs ====================================================

  // Iron Graph
  monthChartIron(divId, info) {
    var ctx = document.getElementById(divId);
    var newInfo = [info[3], info[2], info[6], info[7], info[4], info[5], info[0], info[11], info[10], info[9], info[8], info[1]]
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
                label: 'Blood Results',
                data: newInfo,
                backgroundColor: [
                    'rgba(245, 66, 66, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(245, 66, 66, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(245, 66, 66, 1)',
                    'rgba(245, 66, 66, 1)',
                    'rgba(245, 66, 66, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)'
                ],
                borderColor: [
                    'rgba(245, 66, 66, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(245, 66, 66, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(245, 66, 66, 1)',
                    'rgba(245, 66, 66, 1)',
                    'rgba(245, 66, 66, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
          legend: {
        display: false
    },
    title: {
            display: true,
            text: 'Iron',
            fontSize: 16,
        },
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMin: 20,
                        suggestedMax: 40,
                    }
                }]
            }
        }
    });
  }

  // VitaminD Chart
  monthChartVitamin(divId, info) {
    var ctx = document.getElementById(divId);
    var newInfo = [info[3], info[2], info[6], info[7], info[4], info[5], info[0], info[11], info[10], info[9], info[8], info[1]]
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
                label: 'Blood Results',
                data: newInfo,
                backgroundColor: [
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)'
                ],
                borderColor: [
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
          legend: {
        display: false
    },
    title: {
            display: true,
            text: 'Vitamin D',
            fontSize: 16,
        },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
            }
        }
    });
  }

  // Glucose Chart
  createGlucoseChart(divId, info) {
    var ctx = document.getElementById(divId);
    var newInfo = [info[1], info[5], info[6], info[4], info[0], info[2], info[3]]
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            datasets: [{
                label: 'Blood Results',
                data: newInfo,
                backgroundColor: [
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(245, 66, 66, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(245, 66, 66, 1)',
                    'rgba(0, 230, 84, 1)'
                ],
                borderColor: [
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(245, 66, 66, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(245, 66, 66, 1)',
                    'rgba(0, 230, 84, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
          legend: {
        display: false
    },
    title: {
            display: true,
            text: 'Glucose Blood Sugar',
            fontSize: 16,
        },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
            }
        }
    });
  }

  // Insulin Chart
  createInsulinChart(divId, info) {
    var ctx = document.getElementById(divId);
    var newInfo = [info[1], info[5], info[6], info[4], info[0], info[2], info[3]]
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            datasets: [{
                label: 'Blood Results',
                data: newInfo,
                backgroundColor: [
                    'rgba(255, 162, 0, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(0, 230, 84, 1)'
                ],
                borderColor: [
                    'rgba(255, 162, 0, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(0, 230, 84, 1)',
                    'rgba(255, 162, 0, 1)',
                    'rgba(0, 230, 84, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
          legend: {
        display: false
    },
    title: {
            display: true,
            text: 'Insulin',
            fontSize: 16,
        },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
            }
        }
    });
  }

  // Fetch Data From Database ===========================================================

  getGlucoseDataFromDatabase() {
    this.afd.list('/Blood/Glucose/').valueChanges().subscribe(
      data => {
        this.glucoseData = data;
      }
    )
  }
  getInsulinDataFromDatabase() {
    this.afd.list('/Blood/Insulin/').valueChanges().subscribe(
      data => {
        this.insulinData = data;
      }
    )
  }
  getIronDataFromDatabase() {
    this.afd.list('/Blood/Iron/').valueChanges().subscribe(
      data => {
        this.ironData = data;
      }
    )
  }
  getVitaminDataFromDatabase() {
    this.afd.list('/Blood/VitaminD/').valueChanges().subscribe(
      data => {
        this.vitaminData = data;
      }
    )
  }

}
