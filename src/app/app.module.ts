import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireStorageModule } from '@angular/fire/storage';

var firebaseConfig = {
  apiKey: "AIzaSyAdsvsSIHh0Dk35549MJlZM9YgkBYCQRKQ",
  authDomain: "deco3801-4fa26.firebaseapp.com",
  databaseURL: "https://deco3801-4fa26.firebaseio.com",
  projectId: "deco3801-4fa26",
  storageBucket: "deco3801-4fa26.appspot.com",
  messagingSenderId: "376206271934",
  appId: "1:376206271934:web:cbe5a9bc369fcc205a45ef",
  measurementId: "G-PHY1HDGHJM"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
